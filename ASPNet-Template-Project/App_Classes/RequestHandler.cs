﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FFWD
{
	public class RequestHandler :IHttpModule
	{
		////////////////////////////////////////////////////////////////////////////////////////////
		// NOTE: This request handler has not been installed yet. 
		// Add the following line to the <modules> section of your Web.config file:
		/*
			<add name="RequestHandler" type="FFWD.RequestHandler" preCondition="managedHandler" />
		*/
		////////////////////////////////////////////////////////////////////////////////////////////

		public void Init(HttpApplication context)
		{
			context.BeginRequest += Application_BeginRequest;
			context.EndRequest += Application_EndRequest;
			context.Error += Application_Error;
		}

		private void Application_Error(object sender, EventArgs eventArgs)
		{
			
		}

		private void Application_EndRequest(object sender, EventArgs eventArgs)
		{
			
		}

		private void Application_BeginRequest(object sender, EventArgs eventArgs)
		{

		}

		public void Dispose()
		{

		}
	}
}