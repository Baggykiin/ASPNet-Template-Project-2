﻿/*
using System;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace FFWD
{
	public class UmbracoEventHandler : ApplicationEventHandler
	{
		protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
		{
			// 
			var enabled = Convert.ToBoolean(System.Web.Configuration.WebConfigurationManager.AppSettings["ffwd.umbracoEventHandler.enabled"]);
			if (!enabled) return;

			ContentService.Saving += OnSaving;
			ContentService.Saved += OnSaved;
			ContentService.Copying += OnCopying;
			ContentService.Copied += OnCopied;
			ContentService.Trashing += OnTrashing;
			ContentService.Trashed += OnTrashed;
			ContentService.Deleting += OnDeleting;
			ContentService.Deleted += OnDeleted;
			ContentService.Moving += OnMoving;
			ContentService.Moved += OnMoved;
		}

		private void OnSaving(IContentService sender, SaveEventArgs<IContent> e)
		{

		}

		private void OnSaved(IContentService sender, SaveEventArgs<IContent> e)
		{

		}

		private void OnCopying(IContentService sender, CopyEventArgs<IContent> e)
		{

		}

		private void OnCopied(IContentService sender, CopyEventArgs<IContent> e)
		{

		}

		private void OnTrashing(IContentService sender, MoveEventArgs<IContent> e)
		{

		}

		private void OnTrashed(IContentService sender, MoveEventArgs<IContent> e)
		{

		}

		private void OnDeleting(IContentService sender, DeleteEventArgs<IContent> e)
		{

		}

		private void OnDeleted(IContentService sender, DeleteEventArgs<IContent> e)
		{

		}

		private void OnMoving(IContentService sender, MoveEventArgs<IContent> e)
		{

		}

		private void OnMoved(IContentService sender, MoveEventArgs<IContent> e)
		{

		}
	}
}
*/