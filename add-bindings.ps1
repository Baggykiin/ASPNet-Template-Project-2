echo "Adding dev URL bindings for dev domain {dev_url}"

New-Webbinding {new_name} -Protocol http -Port 80 -HostHeader "{dev_url}.martijn.dev"
New-Webbinding {new_name} -Protocol http -Port 80 -HostHeader "{dev_url}.johan.dev"
New-Webbinding {new_name} -Protocol http -Port 80 -HostHeader "{dev_url}.marina.dev"
